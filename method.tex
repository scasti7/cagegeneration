% !TEX root = main.tex

\section{Method Overview}
%The pipeline is quite simple: our method takes as input a 3D triangle mesh, its curve skeleton and a base complex. We define a mapping function between base complex and input mesh and vice-versa. Some nodes of the curve skeleton have been labelled from the user as "articulation". Using such nodes we refine the base complex splitting it in correspondence of that nodes. The mapping function makes the base complex adhering to the input mesh surface. After refinement we have to inflate the cage as constructed to guarantee that it envelope the original model.

%The input mesh, its curve skeleton and the base complex obtained from \cite{usai2015extraction}. Then the base complex is refined using the curve skeleton nodes has been labelled. After the refinement the base complex, which adheres to the input mesh surface, is inflated in order to guarantee that there are not intersections between them.

\begin{figure}[htb]
	\centering
	% the following command controls the width of the embedded PS file
	% (relative to the width of the current column)
	\includegraphics[width=1\linewidth]{images/pipeline_full2}
	\caption{The pipeline is quite simple: our method takes as input a 3D triangle mesh, its curve skeleton and a base complex, which is obtained with \cite{usai2015extraction}. We define a mapping function between base complex and input mesh and vice-versa. Some nodes of the curve skeleton have been labelled from the user as articulation. Using such nodes we refine the base complex splitting it in correspondence of that nodes. The mapping function makes the base complex adhering to the input mesh surface. After refinement we have to inflate the cage as constructed to guarantee that it envelope the original model.
	}
	%
\end{figure}

The input of our method is a 3D triangle mesh representing an articulated model, its curve skeleton extracted with one of state of art skeletonization method or a simplified skeleton edited using the tool developed by \cite{barbieri2015skeleton} and a base complex which is the one proposed by \cite{usai2015extraction}.
Such base complex follows the intrinsic structure of the given model because it has been extracted from the original model using its curve skeleton. The base complex is a coarse 3D quadrilateral mesh and in addition to this a structured domain for UV mapping and parametrization is given. We use such parametrization to link the base complex to the original model and vice-versa 

\begin{figure}[htb]
	\centering
	% the following command controls the width of the embedded PS file
	% (relative to the width of the current column)
	\includegraphics[width=.9\linewidth]{images/mapping}
	\caption{
		 The structured domain (u,v Parametrization) is used to map each point of the input mesh toward the base complex and the inverse mapping gives us the position of each point of the base complex inside the input mesh
	}
	%
\end{figure}


The newness of our method is given by the use of the curve skeleton to guide the cage refinement. The curve skeleton reflects the shape of the model from which it is extracted. In our opinion, this approach could be the best choice in order to catch meaningful parts of the given model, and also to give us an opportunity to combine different kind of animation: in fact, keeping both skeleton and cage could allow to switch between cage-based animation and skeletal-based animation, thus taking the advantages of both approaches. As well known, there is not a method preferable than the other. Both are used in the same way, it only depends on the type of deformation we aim to do and the model we are going to animate. For instance, we can not imagine to deform a sphere using a skeleton while it is better to use it to move articulation, because skeletons are an abstraction of the real skeleton of the object.

\begin{figure}[htb]
	\centering
	% the following command controls the width of the embedded PS file
	% (relative to the width of the current column)
	\includegraphics[width=.5\linewidth]{images/skel_label}
	\caption{
		The user labelled that node which recognises as "articulation"
	}
	%
\end{figure}


We have defined a method to refine the base complex which follows the curve skeleton trend: some skeletal nodes has been labelled from the user as "articulation node" and they are used to guide the split of the base complex thus constructing our cage for animation. For each articulation node, we find a \textit{cut plane} passing through it, which is orthogonal to skeleton trend in that point. 

\begin{figure}[htb]
	\centering
	% the following command controls the width of the embedded PS file
	% (relative to the width of the current column)
	\includegraphics[width=.9\linewidth]{images/split}
	\caption{
		For each node labelled as articulation, we trace a cut slide plane which follow the curve skeleton trend in correspondence to this point. 
	}
	%
\end{figure}

Each cut plane intersect the base complex creating a subdivision in it. We consider only the intersections that makes sense for our purpose, rejecting the ones which intersect portions of the base complex too far from the articulation node or that are not useful to the refinement process. We make the base complex adhering to the original mesh surface: we project the points obtained from the intersection between the base complex and the "cutting planes" on the mesh surface using the UV parametrization and a mapping function. We use the UV parametrization to find the displacement of each point of the original mesh upon the base complex surface. For each point of the original mesh we know in which quadrilateral of the base complex or better in which face it is mapped and two scalars u,v that represent the displacement in that face. We use this type of interpolation to map the original mesh in the base complex. Let a,b,c,d be the vertices of the face in which the point p is mapped, the new position $ p_{m} $ of that point into that face is given by:\\\\
$ p_{m} = [a*(1-u) + b*u] *(1-v) + [d*(1-u)+c*u]*v $
\\\\
The inverse mapping allows us to map each point of the base complex toward the original mesh surface. In particular, during the refinement, for each intersection point we have to find the position on the input mesh surface. The inverse mapping is made using barycentric coordinates, identifying in which triangle of the input mesh that point lies and projecting it over the input mesh surface. This is done because the base complex as to be as close as possible to the input mesh, to catch meaningful parts. Therefore we let the base complex adhere to the input mesh surface.

%During the refinement, when we compute the intersection between the cut planes and the base complex, we find a set of intersection points. We project each one of this points on the input mesh surface.
%
%The parametrization gives us the information to draw the input mesh 


After the projection we update the base complex adding the new faces and updating all faces which are cut by the plane. Once the base complex has been refined, we have obtained a cage which is coarse enough to be used for animation purpose but it has self-intersections with the input mesh, thus violating one of most important properties that a cage should have. To avoid self-intersections and guarantee that the cage envelope the original model, we inflate the refined base complex using the algorithm developed by \cite{sacht2015nested}. This algorithm works as follow: it define a finer layer and a coarser layer, where the input mesh is used as finer layer while the coarser one is  made by a decimation of the input mesh. It flows the fine mesh until it is fully inside the coarse mesh, and then re-inflates the fine mesh to its original embedding while pushing the coarse mesh out of the way. For our purposes, we use the input mesh as finer layer but the coarser layer is not the decimation of the finer layer but we use the base complex refined in the previous step of the pipeline. This ensure that there are not intersection between the input mesh and the cage constructed with our method.

Unfortunately, this method is not guaranteed to always succeed, so there are some cases in which, due to the inflation step, the entire method does not work.